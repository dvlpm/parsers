package queue

import (
	"fmt"
	"github.com/go-redis/redis"
	"prs/common/model"
	"prs/common/service"
)

type ProductsForPriceUpdateQueue struct {
	Source model.Source
	Client redis.Client
}

func NewProductsForPriceUpdateQueue(sl *service.ServiceLocator, source model.Source) ProductsForPriceUpdateQueue {
	var client redis.Client
	sl.Get(&client)

	return ProductsForPriceUpdateQueue{
		Source: source,
		Client: client,
	}
}

// TODO - make queue messages unique
// TODO - add possibility to delay adding new messages (method Bury)
func (q ProductsForPriceUpdateQueue) Add(product model.Product) {
	fmt.Println(`ProductsForPriceUpdateQueue:Add`, product)
	q.Client.Publish(q.Source.Domain, product.Uuid)
}

func (q ProductsForPriceUpdateQueue) Pop() model.Product {
	fmt.Println(`ProductsForPriceUpdateQueue:Pop`)
	pubSub := q.Client.Subscribe(q.Source.Domain)
	msg := <-pubSub.Channel()
	productUuid := msg.Payload
	return model.Product{Uuid: productUuid}
}
