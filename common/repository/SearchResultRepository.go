package repository

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"prs/common/model"
)

type SearchResultRepository struct {
	DB *gorm.DB
}

func NewSearchResultRepository(db *gorm.DB) SearchResultRepository {
	return SearchResultRepository{DB: db}
}

func (r SearchResultRepository) Save(searchResult model.SearchResult) {
	r.DB.Create(&searchResult)
}

func (r SearchResultRepository) FindOneBySourceAndProduct(
	source model.Source,
	product model.Product,
) model.SearchResult {
	fmt.Println(`SearchResultRepository:FindOneBySourceAndProduct`, source, product)
	searchResult := model.SearchResult{}

	if product.Uuid == `` {
		fmt.Println(`ERROR: invalid product`, product)

		return searchResult
	}

	if product.ID == 0 {
		r.DB.First(&product, `uuid = ?`, product.Uuid)
	}

	r.DB.Preload(`Product`).Preload(`Source`).First(
		&searchResult,
		`source_id = ? AND product_id = ?`,
		source.ID,
		product.ID,
	)

	return searchResult
}
