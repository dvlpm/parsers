FROM golang:alpine

COPY . /go/src/prs
WORKDIR /go/src/prs

RUN \
       apk add --no-cache bash git openssh && \
       go get -u github.com/go-redis/redis && \
       go get -u github.com/jinzhu/gorm && \
       go get -u github.com/jinzhu/gorm/dialects/postgres && \
       go get -u github.com/joho/godotenv && \
       go get -u github.com/gocolly/colly

CMD ["go","run","main.go"]
