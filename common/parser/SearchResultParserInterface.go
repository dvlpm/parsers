package parser

import "prs/common/model"

type SearchResultParserInterface interface {
	ParseSearchResult(product model.Product) model.SearchResult
}
