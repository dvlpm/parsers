package runner

import (
	"fmt"
	"prs/common/client"
	"prs/common/model"
	"prs/common/queue"
	"prs/common/service"
	"time"
)

type GetProductsForPriceUpdateRunner struct {
	PriceHistoryStorageClient   client.PriceHistoryStorageClient
	ProductsForPriceUpdateQueue queue.ProductsForPriceUpdateQueue
	DefaultSource               model.Source
	PeriodInSeconds             int
}

func NewGetProductsForPriceUpdateRunner(
	sl service.ServiceLocator,
	queue queue.ProductsForPriceUpdateQueue,
	defaultSource model.Source,
	periodInSeconds int,
) GetProductsForPriceUpdateRunner {
	var client client.PriceHistoryStorageClient
	sl.Get(&client)

	return GetProductsForPriceUpdateRunner{
		ProductsForPriceUpdateQueue: queue,
		PriceHistoryStorageClient:   client,
		DefaultSource:               defaultSource,
		PeriodInSeconds:             periodInSeconds,
	}
}

func (r GetProductsForPriceUpdateRunner) Run() {
	fmt.Println(`GetProductsForPriceUpdateRunner:Run`)
	ticker := time.NewTicker(time.Duration(r.PeriodInSeconds) * time.Second)

	r.run()
	for {
		select {
		case <-ticker.C:
			r.run()
		}
	}
}

func (r GetProductsForPriceUpdateRunner) run() {
	fmt.Println(`GetProductsForPriceUpdateRunner:run`)
	products := r.PriceHistoryStorageClient.GetProductsBySource(r.DefaultSource)

	for _, product := range products {
		r.ProductsForPriceUpdateQueue.Add(product)
	}
}
