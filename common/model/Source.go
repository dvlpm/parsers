package model

import (
	"github.com/jinzhu/gorm"
	"strings"
)

type Source struct {
	gorm.Model
	Domain string `gorm:"primary_key;unique;" json:"domain"`
}

func (s Source) GetDomainAsPath() string {
	replacer := strings.NewReplacer(`.`, `_`, `-`, `_`)

	return replacer.Replace(s.Domain)
}

func (s Source) GetDomainWithHttps() string {
	return `https://` + s.Domain + `/`
}
