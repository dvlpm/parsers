package service

import (
	"reflect"
)

type ServiceLocator struct {
	services map[string]interface{}
}

func (s *ServiceLocator) Register(some interface{}) {
	if s.services == nil {
		s.services = make(map[string]interface{})
	}

	key := reflect.TypeOf(some).String()

	s.services[key] = some
}

func (s *ServiceLocator) Get(some interface{}) bool {
	k := reflect.TypeOf(some).Elem()
	kind := k.Kind()

	if kind == reflect.Ptr {
		k = k.Elem()
		kind = k.Kind()
	}

	key := reflect.TypeOf(some).Elem().String()
	service := s.services[key]

	if kind == reflect.Interface {
		reflect.ValueOf(some).Elem().Set(reflect.ValueOf(service))
		return true
	}

	reflect.Indirect(
		reflect.ValueOf(some),
	).Set(reflect.ValueOf(service))

	return true
}
