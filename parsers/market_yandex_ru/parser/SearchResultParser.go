package parser

import (
	"fmt"
	"github.com/gocolly/colly"
	"net/url"
	"prs/common/model"
	"prs/common/service"
	"strings"
)

type SearchResultParser struct {
	Source model.Source
	Parser service.Parser
}

func (s SearchResultParser) ParseSearchResult(product model.Product) model.SearchResult {
	fmt.Println(`market_yandex_ru/SearchResultParserInterface:ParseSearchResult`, product)
	searchResult := model.SearchResult{Product: product, Source: s.Source}

	for _, nameAlias := range product.NameAliases {
		searchByNameAliasUrl := s.Source.GetDomainWithHttps() + `search?text=` + url.QueryEscape(nameAlias)

		s.Parser.OnHTML(`.n-snippet-card2:first-child`, func(e *colly.HTMLElement) {
			id := strings.Replace(e.Attr(`id`), `product-`, ``, -1)

			searchResult.Url = s.Source.GetDomainWithHttps() + `product/` + id
			searchResult.IsSuccess = true
		})

		err := s.Parser.Get(searchByNameAliasUrl)

		fmt.Println(err)

		if searchResult.IsSuccess {
			break
		}
	}

	return searchResult
}
