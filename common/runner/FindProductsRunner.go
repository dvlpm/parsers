package runner

import (
	"fmt"
	"prs/common/client"
	"prs/common/model"
	"prs/common/parser"
	"prs/common/queue"
	"prs/common/repository"
	"prs/common/service"
	"time"
)

type FindProductsRunner struct {
	ProductRepository           repository.ProductRepository
	SearchResultRepository      repository.SearchResultRepository
	CatalogClient               client.CatalogClient
	ProductSearcher             parser.SearchResultParserInterface
	ProductsForPriceUpdateQueue queue.ProductsForPriceUpdateQueue
	DefaultSource               model.Source
	PeriodInSeconds             int
}

func NewFindProductsRunner(
	sl service.ServiceLocator,
	productSearcher parser.SearchResultParserInterface,
	queue queue.ProductsForPriceUpdateQueue,
	defaultSource model.Source,
	periodInSeconds int,
) FindProductsRunner {
	var productRepository repository.ProductRepository
	sl.Get(&productRepository)
	var searchResultRepository repository.SearchResultRepository
	sl.Get(&searchResultRepository)
	var catalogClient client.CatalogClient
	sl.Get(&catalogClient)

	return FindProductsRunner{
		ProductRepository:           productRepository,
		SearchResultRepository:      searchResultRepository,
		CatalogClient:               catalogClient,
		ProductSearcher:             productSearcher,
		ProductsForPriceUpdateQueue: queue,
		DefaultSource:               defaultSource,
		PeriodInSeconds:             periodInSeconds,
	}
}

func (r FindProductsRunner) Run() {
	fmt.Println(`FindProductsRunner:Run`)
	ticker := time.NewTicker(time.Duration(r.PeriodInSeconds) * time.Second)

	if r.DefaultSource.ID == 0 {
		panic(`Invalid source`)
	}

	r.run()
	for {
		select {
		case <-ticker.C:
			r.run()
		}
	}
}

func (r FindProductsRunner) run() {
	fmt.Println(`FindProductsRunner:run`)
	products := r.CatalogClient.GetProducts()

	for _, product := range products {
		r.ProductRepository.EnrichByUuid(&product)

		existedSearchResult := r.SearchResultRepository.FindOneBySourceAndProduct(r.DefaultSource, product)

		if existedSearchResult.IsSuccess {
			continue
		}

		searchResult := r.ProductSearcher.ParseSearchResult(product)
		r.SearchResultRepository.Save(searchResult)

		if !searchResult.IsSuccess {
			continue
		}

		r.ProductsForPriceUpdateQueue.Add(product)
	}
}
