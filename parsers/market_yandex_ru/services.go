package market_yandex_ru

import (
	"github.com/gocolly/colly"
	"os"
	"prs/common/dto"
	"prs/common/model"
	"prs/common/queue"
	"prs/common/repository"
	"prs/common/runner"
	"prs/common/service"
	"prs/parsers/market_yandex_ru/parser"
	"strconv"
)

const domain = `market.yandex.ru`

type GetProductForPriceUpdateRunner interface {
	Run()
}

type FindProductsRunner interface {
	Run()
}

type ParsePricesRunner interface {
	Run()
}

func AddServices(sl *service.ServiceLocator, config dto.Config) {
	source := model.Source{Domain: domain}

	var sourceRepository repository.SourceRepository
	sl.Get(&sourceRepository)

	sourceRepository.EnrichByDomain(&source)

	priceUpdateQueue := queue.NewProductsForPriceUpdateQueue(sl, source)

	p, err := strconv.Atoi(os.Getenv(`MARKET_YANDEX_RU_GET_PRODUCTS_FOR_PRICE_UPDATE_PERIOD_IN_SECONDS`))

	if err != nil || p <= 0 {
		panic(err)
	}

	var pfpuRunner GetProductForPriceUpdateRunner
	pfpuRunner = runner.NewGetProductsForPriceUpdateRunner(
		*sl,
		priceUpdateQueue,
		source,
		p,
	)

	sl.Register(&pfpuRunner)

	parserService := service.NewParser(sl, config, colly.NewCollector(), source)

	productSearcher := parser.SearchResultParser{Source: source, Parser: parserService}

	p, err = strconv.Atoi(os.Getenv(`MARKET_YANDEX_RU_FIND_PRODUCTS_PERIOD_IN_SECONDS`))

	if err != nil || p <= 0 {
		panic(err)
	}

	var fpRunner FindProductsRunner
	fpRunner = runner.NewFindProductsRunner(
		*sl,
		productSearcher,
		priceUpdateQueue,
		source,
		p,
	)

	sl.Register(&fpRunner)

	productPriceParser := parser.ProductPricesParser{DefaultSource: source, Parser: parserService}

	var ppRunner ParsePricesRunner
	ppRunner = runner.NewParsePricesRunner(*sl, productPriceParser, priceUpdateQueue, source)

	sl.Register(&ppRunner)
}
