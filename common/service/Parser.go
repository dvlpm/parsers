package service

import (
	"fmt"
	"github.com/gocolly/colly"
	"io/ioutil"
	"net/http"
	"prs/common/dto"
	"prs/common/model"
	"time"
)

type Parser struct {
	Source    model.Source
	Collector *colly.Collector
}

func NewParser(
	sl *ServiceLocator,
	config dto.Config,
	collector *colly.Collector,
	source model.Source,
) Parser {
	collector.MaxBodySize = 99999999999999
	collector.AllowURLRevisit = true
	collector.CacheDir = config.VarDirectory + `cache`
	responsesDir := config.VarDirectory + `log/` + source.GetDomainAsPath() + `/responses`

	collector.OnResponse(func(response *colly.Response) {
		responseFileName := fmt.Sprintf(`%sresponse%d.html`, responsesDir, time.Now().Unix())
		err := ioutil.WriteFile(responseFileName, response.Body, 0644)
		fmt.Println(`ERROR: `, err)
	})

	return Parser{
		Source:    source,
		Collector: collector,
	}
}

func (p *Parser) OnHTML(goQuerySelector string, f colly.HTMLCallback) {
	p.Collector.OnHTML(goQuerySelector, f)
}

func (p *Parser) Get(URL string) error {
	return p.Collector.Request(
		http.MethodGet,
		URL,
		nil,
		nil,
		p.getCollectorHeaders(),
	)
}

func (p Parser) getCollectorHeaders() http.Header {
	hdr := http.Header{}

	hdr.Add(`Host`, p.Source.Domain)
	hdr.Add(`Pragma`, `no-cache`)
	hdr.Add(`Cache-Control`, `no-cache`)
	hdr.Add(`Accept-Encoding`, `gzip, deflate`)
	hdr.Add(`Connection`, `keep-alive`)
	hdr.Add(`User-Agent`, `PostmanRuntime/7.19.0`)

	return hdr
}
