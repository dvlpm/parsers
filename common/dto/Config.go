package dto

type Config struct {
	PriceHistoryStorageBaseUrl string
	CatalogBaseUrl             string
	RedisAddr                  string
	GormArgs                   string
	VarDirectory               string
}
