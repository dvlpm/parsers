package runner

import (
	"fmt"
	"prs/common/client"
	"prs/common/model"
	"prs/common/parser"
	"prs/common/queue"
	"prs/common/repository"
	"prs/common/service"
)

type ParsePricesRunner struct {
	PriceHistoryStorageClient   client.PriceHistoryStorageClient
	SearchResultRepository      repository.SearchResultRepository
	ProductPriceParser          parser.ProductPricesParserInterface
	ProductsForPriceUpdateQueue queue.ProductsForPriceUpdateQueue
	DefaultSource               model.Source
}

func NewParsePricesRunner(
	sl service.ServiceLocator,
	parser parser.ProductPricesParserInterface,
	queue queue.ProductsForPriceUpdateQueue,
	defaultSource model.Source,
) ParsePricesRunner {
	var client client.PriceHistoryStorageClient
	sl.Get(&client)
	var repository repository.SearchResultRepository
	sl.Get(&repository)

	return ParsePricesRunner{
		PriceHistoryStorageClient:   client,
		ProductPriceParser:          parser,
		SearchResultRepository:      repository,
		ProductsForPriceUpdateQueue: queue,
		DefaultSource:               defaultSource,
	}
}

func (r ParsePricesRunner) Run() {
	fmt.Println(`ParsePricesRunner:Run`)
	for {
		product := r.ProductsForPriceUpdateQueue.Pop()

		searchResult := r.SearchResultRepository.FindOneBySourceAndProduct(r.DefaultSource, product)

		if !searchResult.IsSuccess {
			r.ProductsForPriceUpdateQueue.Add(product)
			continue
		}

		prices := r.ProductPriceParser.ParseProductPrices(searchResult)

		for _, price := range prices {
			r.PriceHistoryStorageClient.SavePrice(price)
		}
	}
}
