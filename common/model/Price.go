package model

// TODO change discount on UnactualValue after changes in PHS
type Price struct {
	Source   Source  `json:"source"`
	Product  Product `json:"product"`
	Value    float64 `json:"value"`
	Discount int     `json:"discount"`
}
