package parser

import "prs/common/model"

type ProductPricesParserInterface interface {
	ParseProductPrices(searchResult model.SearchResult) []model.Price
}
