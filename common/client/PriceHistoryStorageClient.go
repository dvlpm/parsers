package client

import (
	"fmt"
	"prs/common/model"
)

type PriceHistoryStorageClient struct {
	BaseUrl string
}

func (c PriceHistoryStorageClient) GetProductsBySource(source model.Source) []model.Product {
	fmt.Println(`PriceHistoryStorageClient:GetProductsBySource`, source)
	url := c.BaseUrl + `source/` + source.Domain + `/products-for-update`

	var products []model.Product

	err := get(url, &products)

	if err != nil {
		return []model.Product{}
	}

	return products
}

func (c PriceHistoryStorageClient) SavePrice(price model.Price) {
	fmt.Println(`PriceHistoryStorageClient:SavePrice`, price)
	url := c.BaseUrl + `prices`

	err := post(url, price)

	if err != nil {
		return
	}
}
