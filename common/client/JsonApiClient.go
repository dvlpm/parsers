package client

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"
)

var myClient = &http.Client{Timeout: 10 * time.Second}

func get(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	decoder := json.NewDecoder(r.Body)

	return decoder.Decode(target)
}

func post(url string, request interface{}) error {
	requestBody, err := json.Marshal(request)
	r, err := myClient.Post(url, `application/json`, bytes.NewBuffer(requestBody))
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return nil
}
