package _dto

import "strconv"

type OfferDetailsDto struct {
	Details struct {
		Offer struct {
			Shop struct {
				DomainUrl string
			}
			Price struct {
				Value    string
				Discount struct {
					OldMin string
				}
			} `json:"prices"`
		}
	} `json:"n-i-offer-details-button"`
}

func (o OfferDetailsDto) GetDomainUrl() string {
	return o.Details.Offer.Shop.DomainUrl
}

func (o OfferDetailsDto) GetPriceValue() float64 {
	priceValue, err := strconv.ParseFloat(o.Details.Offer.Price.Value, 64)

	if err != nil {
		return 0
	}

	return priceValue
}

func (o OfferDetailsDto) GetUnactualPriceValue() float64 {
	unactualPriceValue, err := strconv.ParseFloat(o.Details.Offer.Price.Discount.OldMin, 64)

	if err != nil {
		return 0
	}

	return unactualPriceValue
}
