package client

import (
	"fmt"
	"prs/common/model"
)

type CatalogClient struct {
	BaseUrl string
}

func (c CatalogClient) GetProducts() []model.Product {
	fmt.Println(`CatalogClient:GetProducts`)
	url := c.BaseUrl + `products?orderBy[id]=DESC`

	var products []model.Product

	err := get(url, &products)

	if err != nil {
		return []model.Product{}
	}

	return products
}
