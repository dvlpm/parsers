package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
	"log"
	"os"
	"prs/common"
	"prs/common/dto"
	"prs/common/model"
	"prs/common/runner"
	"prs/common/service"
	"prs/parsers/market_yandex_ru"
	"sync"
)

func main() {
	sl := service.ServiceLocator{}

	config := createConfig()

	initServices(&sl, config)
	initMigrations(&sl)

	runners := createRunners(&sl)

	var wg sync.WaitGroup

	wg.Add(len(runners))

	for _, r := range runners {
		go func(runner runner.RunnerInterface) {
			defer wg.Done()
			runner.Run()
		}(r)
	}

	wg.Wait()
}

func createConfig() dto.Config {
	dir := `./`

	err := godotenv.Load(dir + `.env`)
	if err != nil {
		log.Fatal(`Error loading .env ⁄ile`)
	}

	appMode := os.Getenv(`APP_MODE`)

	redisAddr := os.Getenv(`REDIS_ADDR`)
	gormArgs := os.Getenv(`DB_CONNECTION`)

	if appMode == `debug` {
		redisAddr = os.Getenv(`DEBUG_REDIS_ADDR`)
		gormArgs = os.Getenv(`DEBUG_DB_CONNECTION`)
	}

	config := dto.Config{
		VarDirectory:               dir + os.Getenv(`VAR_DIRECTORY`),
		PriceHistoryStorageBaseUrl: os.Getenv(`PRICE_HISTORY_STORAGE_BASE_URL`),
		CatalogBaseUrl:             os.Getenv(`CATALOG_BASE_URL`),
		RedisAddr:                  redisAddr,
		GormArgs:                   gormArgs,
	}

	return config
}

func initServices(sl *service.ServiceLocator, config dto.Config) {
	common.AddServices(sl, config)
	market_yandex_ru.AddServices(sl, config)
}

func initMigrations(sl *service.ServiceLocator) {
	var db gorm.DB
	sl.Get(&db)

	db.AutoMigrate(&model.Product{}, &model.SearchResult{}, &model.Source{})
}

func createRunners(sl *service.ServiceLocator) []runner.RunnerInterface {
	var yandexGPFPURunner *market_yandex_ru.GetProductForPriceUpdateRunner
	sl.Get(&yandexGPFPURunner)
	var yandexFPRunner *market_yandex_ru.FindProductsRunner
	sl.Get(&yandexFPRunner)
	var yandexPPRunner *market_yandex_ru.ParsePricesRunner
	sl.Get(&yandexPPRunner)

	return []runner.RunnerInterface{
		*yandexFPRunner,
		*yandexGPFPURunner,
		*yandexPPRunner,
	}
}
