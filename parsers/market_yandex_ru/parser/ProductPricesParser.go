package parser

import (
	"encoding/json"
	"fmt"
	"github.com/gocolly/colly"
	"prs/common/model"
	"prs/common/service"
	d "prs/parsers/market_yandex_ru/dto"
)

type ProductPricesParser struct {
	DefaultSource model.Source
	Parser        service.Parser
}

func (p ProductPricesParser) ParseProductPrices(searchResult model.SearchResult) []model.Price {
	fmt.Println(`market_yandex_ru/ProductPricesParserInterface:ParseProductPrices`, searchResult)
	var prices []model.Price
	var valuesSum float64
	var valuesCount int
	isLastPage := false
	maxPageCount := 20
	currentPageNumber := 1

	offersUrl := searchResult.Url + `/offers?how=aprice`

	p.Parser.OnHTML(`.n-snippet-card`, func(e *colly.HTMLElement) {
		isLastPage = true
		dataBemString := e.ChildAttr(`.n-snippet-card__image`, `data-bem`)
		var offerDetailsDto d.OfferDetailsDto
		err := json.Unmarshal([]byte(dataBemString), &offerDetailsDto)

		if err != nil {
			fmt.Println(`WARN: can't parse data`, err)
			return
		}

		discount := float64(0)
		value := offerDetailsDto.GetPriceValue()
		valuesSum += value
		valuesCount++
		unactualValue := offerDetailsDto.GetUnactualPriceValue()

		if unactualValue > 0 {
			discount = 100 - value*100/unactualValue
		}

		prices = append(prices, model.Price{
			Source: model.Source{
				Domain: offerDetailsDto.GetDomainUrl(),
			},
			Product:  searchResult.Product,
			Value:    value,
			Discount: int(discount),
		})
	})

	p.Parser.OnHTML(`.n-pager__button-next`, func(e *colly.HTMLElement) {
		isLastPage = false
	})

	for isLastPage == false {
		offerUrlWithPage := offersUrl + fmt.Sprintf(`&page=%d`, currentPageNumber)
		err := p.Parser.Get(offerUrlWithPage)
		currentPageNumber++

		if err != nil {
			fmt.Println(`ERROR: `, err)
		}

		if currentPageNumber > maxPageCount {
			fmt.Println(`WARNING: max page count exceeded`)
			break
		}
	}

	if len(prices) > 0 {
		prices = append(prices, model.Price{
			Source:  p.DefaultSource,
			Product: searchResult.Product,
			Value:   valuesSum / float64(valuesCount),
		})
	}

	return prices
}
