package runner

type RunnerInterface interface {
	Run()
}
