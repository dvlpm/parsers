package model

import "github.com/jinzhu/gorm"

type SearchResult struct {
	gorm.Model
	Product   Product
	ProductID int
	Url       string
	Source    Source
	SourceID  int
	IsSuccess bool
}
