package model

import "github.com/jinzhu/gorm"

type Product struct {
	gorm.Model
	Uuid        string `gorm:"type:uuid;primary_key;unique;" json:"uuid"`
	Name        string
	NameAliases []string `gorm:"-"`
}
