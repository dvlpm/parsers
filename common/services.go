package common

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"prs/common/client"
	"prs/common/dto"
	"prs/common/repository"
	"prs/common/service"
)

func AddServices(sl *service.ServiceLocator, config dto.Config) {
	rd := redis.NewClient(&redis.Options{
		Addr:     config.RedisAddr,
		Password: ``,
		DB:       0,
	})

	sl.Register(*rd)

	db, err := gorm.Open(
		`postgres`,
		config.GormArgs,
	)

	if err != nil {
		fmt.Println(err)
		panic(`failed to connect database`)
	}

	sl.Register(*db)

	sl.Register(client.PriceHistoryStorageClient{BaseUrl: config.PriceHistoryStorageBaseUrl})
	sl.Register(client.CatalogClient{BaseUrl: config.CatalogBaseUrl})

	pr := repository.NewProductRepository(db)

	sl.Register(pr)

	srr := repository.NewSearchResultRepository(db)

	sl.Register(srr)

	sr := repository.NewSourceRepository(db)

	sl.Register(sr)
}
