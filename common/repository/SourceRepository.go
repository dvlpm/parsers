package repository

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"prs/common/model"
)

type SourceRepository struct {
	DB *gorm.DB
}

func NewSourceRepository(db *gorm.DB) SourceRepository {
	return SourceRepository{DB: db}
}

func (r SourceRepository) EnrichByDomain(source *model.Source) {
	existedSource := r.FindOneByDomain(source.Domain)

	if existedSource.ID == 0 {
		r.Save(source)
	} else {
		*source = existedSource
	}
}

func (r SourceRepository) FindOneByDomain(domain string) model.Source {
	fmt.Println(`SourceRepository:FindOneByDomain`, domain)
	source := model.Source{}

	r.DB.First(&source, `domain = ?`, domain)

	return source
}

func (r SourceRepository) Save(source *model.Source) {
	fmt.Println(`SourceRepository:Save`, source)
	r.DB.Create(&source)
}
