package repository

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"prs/common/model"
)

type ProductRepository struct {
	DB *gorm.DB
}

func NewProductRepository(db *gorm.DB) ProductRepository {
	return ProductRepository{DB: db}
}

func (r ProductRepository) EnrichByUuid(product *model.Product) {
	existedProduct := r.FindOneByUuid(product.Uuid)

	if existedProduct.ID == 0 {
		r.Save(product)
	} else {
		*product = existedProduct
	}
}

func (r ProductRepository) FindOneByUuid(uuid string) model.Product {
	fmt.Println(`ProductRepository:FindOneByUuid`, uuid)
	product := model.Product{}

	r.DB.First(&product, `uuid = ?`, uuid)

	return product
}

func (r ProductRepository) Save(product *model.Product) {
	fmt.Println(`ProductRepository:Save`, product)
	r.DB.Create(&product)
}
